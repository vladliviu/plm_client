-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: plm
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `bom`
--

LOCK TABLES `bom` WRITE;
/*!40000 ALTER TABLE `bom` DISABLE KEYS */;
/*!40000 ALTER TABLE `bom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `downtime`
--

LOCK TABLES `downtime` WRITE;
/*!40000 ALTER TABLE `downtime` DISABLE KEYS */;
/*!40000 ALTER TABLE `downtime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (9,'Laser sintering machine','The Lasercusing machine is used for the production of medical and dental products along with some jewelry application in a elaborate structure.','135000','https://img.directindustry.com/images_di/photo-mg/15662-11748667.jpg','Laser sintering machines','2020-07-01 11:32:32',NULL),(10,'INSPECTION MICROSCOPE / FOR MATERIALS RESEARCH / FOR MATERIALS INSPECTION / MEASURING','Speed matters in inspection, process control, or defect and failure analysis for the microelectronics and semiconductor industry. The faster you detect a defect, the faster you can react.','450000','https://img.directindustry.com/images_di/photo-mg/182601-13175481.jpg','Electronics production machines','2020-07-01 11:32:32',NULL),(11,'PLASMA WAFER ETCHING MACHINE / FOR THE MICROELECTRONICS INDUSTRY','The new HHT 9000 platform utilizes Hitachi’s proprietary low-contaminant, high-speed transfer system for high productivity.','370000','https://img.directindustry.com/images_di/photo-mg/30506-9524284.jpg','Electronics production machines','2020-07-01 11:32:32',NULL),(12,'ALUMINUM THERMAL BREAK PROFILE PRODUCTION LINE / AUTOMATIC','•Loading roller table 2,4 m','678666','https://img.directindustry.com/images_di/photo-mg/27866-12567191.jpg','Production lines','2020-07-01 11:33:49',NULL),(13,'COMPOSITE STONE PRODUCTION LINE','BRETONSTONE® slabs are a collection of molding,strengthening and slab plating mechanisms and treatments.','760000','https://img.directindustry.com/images_di/photo-mg/69464-8209303.jpg','Production lines','2020-07-01 11:34:28',NULL),(14,'METAL PART STRAIGHTENING MACHINE / FOR SHEET METAL','AKYAPAK APSM machine is designed as standard to fix sheet metal thicknesses from 3 mm to 60 mm and widths from 500 mm to 3000 mm.','550000','https://img.directindustry.com/images_di/photo-mg/33663-14570287.jpg','Other production machines','2020-07-01 11:35:26',NULL),(15,'KNIFE MILL / VERTICAL / FOR CABLES / COPPER','The new series of compact granulators SINCRO, has been designed to process copper and aluminum cables and radiators.','250000','https://img.directindustry.com/images_di/photo-mg/63289-15152269.jpg','Mills','2020-07-01 11:36:35',NULL),(16,'AUTOMATIC LABELER / FOR PACKAGING / LINEAR ARRAY / HIGH-SPEED','The MR 635 cross web labeller labels the bottom of a finished thermoforming packaging.','95000','https://img.directindustry.com/images_di/photo-mg/18330-10125901.jpg','Labeler','2020-07-01 11:37:28',NULL);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Wheat','Wheat is a grass widely cultivated for its seed, a cereal grain which is a worldwide staple food. The many species of wheat together make up the genus Triticum','https://cdn11.bigcommerce.com/s-dis4vxtxtc/images/stencil/1280x1280/products/2334/2774/image_1733__47850.1567254918.jpg?c=2&imbypass=on?imbypass=on',10000,0.7,'2020-07-01 12:35:32',NULL,2),(2,'Water','Water is an inorganic, transparent, tasteless, odorless, and nearly colorless chemical substance','https://s1.kaercher-media.com/image/pim/PPT_splash_1_1260x456.jpg',40000,0.1,'2020-07-01 12:36:35',NULL,4),(3,'Yeast','Yeast is a single-cell organism, called Saccharomyces cerevisiae, which needs food, warmth, and moisture to thrive.','https://image.made-in-china.com/2f0j00OmdarLyqQjus/Instant-Dry-Yeast-for-Bread-at-Factory-Price.jpg',1200,2.3,'2020-07-01 12:37:46',NULL,2),(4,'Iron Sheet',' plate iron thinner than tank iron','https://5.imimg.com/data5/XJ/KA/MY-2249119/galvanized-iron-sheet-500x500.jpg',500,150,'2020-07-01 19:10:08',NULL,2);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `productionhistory`
--

LOCK TABLES `productionhistory` WRITE;
/*!40000 ALTER TABLE `productionhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `productionhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'manager1','1234','Liviu','Vlad','vldliviu@gmail.com','Fabricii 105, Cluj-Napoca','0744212421','https://cdn1.iconfinder.com/data/icons/office-and-internet-3/49/248-512.png','2020-07-01 11:07:36',NULL,1),(2,'manager2','1234','Ioan','Crisan','incrisan3@gmail.com','Muncii 44, Cluj-Napoca','0762321421','https://www.pngarts.com/files/3/Employee-Avatar-PNG-Image-with-Transparent-Background.png','2020-07-01 11:07:36',NULL,2),(3,'worker1','1234','Flaviu','Cosma','fcosma@gmail.com','Strada Vulturului 42, Marasti, Cluj-Napoca','0744332452','https://cdn3.iconfinder.com/data/icons/avatar-set/512/Avatar12-512.png','2020-07-01 11:07:36',NULL,5),(4,'worker2','1234','Mihai','Vaida','mv@yahoo.com','Republicii 106 T2, Cluj-Napoca','0736223454','https://www.pngarts.com/files/3/Employee-Avatar-PNG-Image-with-Transparent-Background.png','2020-07-01 11:14:40',NULL,6),(5,'analyst1','1234','Ioana','Mois','imois@gmail.com','Gheorgheni 30, Cluj-Napoca','0747212231','https://cdn1.iconfinder.com/data/icons/office-and-internet-3/49/248-512.png','2020-07-01 11:14:40',NULL,3),(6,'analyst2','1234','Narcisa','Muntean','narmunt@gmail.com','Manastur 43, Cluj-Napoca','0744221345','https://cdn3.iconfinder.com/data/icons/avatar-set/512/Avatar12-512.png','2020-07-01 11:15:29',NULL,4);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `staffoccupation`
--

LOCK TABLES `staffoccupation` WRITE;
/*!40000 ALTER TABLE `staffoccupation` DISABLE KEYS */;
INSERT INTO `staffoccupation` VALUES (1,'Manager 8.1','Developing and managing the shop floor operations, inventory, equipment and workers.',5650,8,'2020-01-01 08:00:00','2020-01-01 16:00:00','2020-01-01 10:58:36',NULL),(2,'Manager 8.2','Developing and managing the shop floor operations, inventory, equipment and workers.',5650,8,'2020-01-01 16:00:00','2020-01-01 00:00:00','2020-07-01 10:59:48',NULL),(3,'Worker 8.1','Trained and educated profesionals tasked with operating the various machines and equipments on the shop floor level',4000,8,'2020-01-01 08:00:00','2020-01-01 16:00:00','2020-07-01 11:03:30',NULL),(4,'Worker 8.2','Trained and educated profesionals tasked with operating the various machines and equipments on the shop floor level',4000,8,'2020-01-01 16:00:00','2020-01-01 00:00:00','2020-07-01 11:03:30',NULL),(5,'Analyst & Sales','Negociates with clients and suppliers and offers detailed reports of the factory\'s performance',4500,8,'2020-01-01 08:00:00','2020-01-01 16:00:00','2020-07-01 11:05:03',NULL),(6,'Analyst & Sales','Negociates with clients and suppliers and offers detailed reports of the factory\'s performance',4500,8,'2020-01-01 16:00:00','2020-01-01 00:00:00','2020-07-01 11:05:03',NULL);
/*!40000 ALTER TABLE `staffoccupation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uom`
--

LOCK TABLES `uom` WRITE;
/*!40000 ALTER TABLE `uom` DISABLE KEYS */;
INSERT INTO `uom` VALUES (1,'gram','1000','mass','2020-07-01 11:18:59',NULL),(2,'kilogram','1','mass','2020-07-01 11:18:59',NULL),(3,'ton','0.001','mass','2020-07-01 11:18:59',NULL),(4,'litre','1','volume','2020-07-01 11:21:26',NULL),(5,'kilolitre','0.001','volume','2020-07-01 11:21:26',NULL),(6,'gallon','3.78','volume','2020-07-01 11:21:26',NULL),(7,'pint','0.47','volume','2020-07-01 11:25:06',NULL),(8,'meter','1','length','2020-07-01 11:25:06',NULL),(9,'centimeter','0.01','length','2020-07-01 11:25:06',NULL),(10,'demetre','10','length','2020-07-01 11:25:06',NULL),(11,'kilometer','1000','length','2020-07-01 11:25:06',NULL);
/*!40000 ALTER TABLE `uom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `waste`
--

LOCK TABLES `waste` WRITE;
/*!40000 ALTER TABLE `waste` DISABLE KEYS */;
/*!40000 ALTER TABLE `waste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workflow`
--

LOCK TABLES `workflow` WRITE;
/*!40000 ALTER TABLE `workflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `workflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workflowstatus`
--

LOCK TABLES `workflowstatus` WRITE;
/*!40000 ALTER TABLE `workflowstatus` DISABLE KEYS */;
INSERT INTO `workflowstatus` VALUES (5,'Design','The workflow is still being designed','2020-07-05 19:09:22',NULL),(6,'Active','The workflow has started','2020-07-05 19:09:22',NULL),(7,'Completed','The workflow has ended','2020-07-05 19:09:22',NULL),(8,'Interupted','The workflow has been canceled','2020-07-05 19:09:22',NULL);
/*!40000 ALTER TABLE `workflowstatus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-05 22:36:58
