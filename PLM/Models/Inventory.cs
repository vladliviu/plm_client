﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Inventory
    {
        public Inventory()
        {
            Waste = new HashSet<Waste>();
        }

        public int Id { get; set; }
        public double? Expectedqty { get; set; }
        public double? Availableqty { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int WorkflowId { get; set; }
        public int WorkflowStaffId { get; set; }
        public int WorkflowBomId { get; set; }
        public int WorkflowBomProductId { get; set; }
        public int WorkflowBomEquipmentId { get; set; }

        public virtual Workflow Workflow { get; set; }
        public virtual ICollection<Waste> Waste { get; set; }
    }
}
