﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Order = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Productcode { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}
