﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Workflow
    {
        public Workflow()
        {
            Inventory = new HashSet<Inventory>();
            Productionhistory = new HashSet<Productionhistory>();
        }

        public int Id { get; set; }
        public int StaffId { get; set; }
        public int BomId { get; set; }
        public int BomProductId { get; set; }
        public int BomEquipmentId { get; set; }
        public string Details { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int WorkflowstatusId { get; set; }
        public int EquipmentId { get; set; }

        public virtual Bom Bom { get; set; }
        public virtual Equipment Equipment { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual Workflowstatus Workflowstatus { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
        public virtual ICollection<Productionhistory> Productionhistory { get; set; }
    }
}
