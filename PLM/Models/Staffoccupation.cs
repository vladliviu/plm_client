﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Staffoccupation
    {
        public Staffoccupation()
        {
            Staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Salary { get; set; }
        public int? ShiftDuration { get; set; }
        public DateTime? ShiftFrom { get; set; }
        public DateTime? ShiftTo { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Staff> Staff { get; set; }
    }
}
