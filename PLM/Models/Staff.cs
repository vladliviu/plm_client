﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Staff
    {
        public Staff()
        {
            Downtime = new HashSet<Downtime>();
            Notification = new HashSet<Notification>();
            Workflow = new HashSet<Workflow>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int StaffOccupationId { get; set; }

        public virtual Staffoccupation StaffOccupation { get; set; }
        public virtual ICollection<Downtime> Downtime { get; set; }
        public virtual ICollection<Notification> Notification { get; set; }
        public virtual ICollection<Workflow> Workflow { get; set; }
    }
}
