﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Product
    {
        public Product()
        {
            Bom = new HashSet<Bom>();
            Order = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public double? Quantity { get; set; }
        public double? Price { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int UomId { get; set; }

        public virtual Uom Uom { get; set; }
        public virtual ICollection<Bom> Bom { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
