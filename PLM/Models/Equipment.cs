﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Equipment
    {
        public Equipment()
        {
            Bom = new HashSet<Bom>();
            Downtime = new HashSet<Downtime>();
            Workflow = new HashSet<Workflow>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string Avatar { get; set; }
        public string Type { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Bom> Bom { get; set; }
        public virtual ICollection<Downtime> Downtime { get; set; }
        public virtual ICollection<Workflow> Workflow { get; set; }
    }
}
