﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Notification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int StaffId { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
