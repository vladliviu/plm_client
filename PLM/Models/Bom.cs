﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Bom
    {
        public Bom()
        {
            Workflow = new HashSet<Workflow>();
        }

        public int Id { get; set; }
        public int ProductId { get; set; }
        public int EquipmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Quantity { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<Workflow> Workflow { get; set; }
    }
}
