﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Productionhistory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Quantiy { get; set; }
        public double? Price { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int WorkflowId { get; set; }
        public int WorkflowStaffId { get; set; }
        public int WorkflowBomId { get; set; }
        public int WorkflowBomProductId { get; set; }
        public int WorkflowBomEquipmentId { get; set; }

        public virtual Workflow Workflow { get; set; }
    }
}
