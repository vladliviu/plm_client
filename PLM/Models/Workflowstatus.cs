﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Workflowstatus
    {
        public Workflowstatus()
        {
            Workflow = new HashSet<Workflow>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Workflow> Workflow { get; set; }
    }
}
