﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Waste
    {
        public int Id { get; set; }
        public string Details { get; set; }
        public double? Quantity { get; set; }
        public double? Value { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int InventoryId { get; set; }

        public virtual Inventory Inventory { get; set; }
    }
}
