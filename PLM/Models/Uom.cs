﻿using System;
using System.Collections.Generic;

namespace PLM.Models
{
    public partial class Uom
    {
        public Uom()
        {
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Units { get; set; }
        public string Type { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
