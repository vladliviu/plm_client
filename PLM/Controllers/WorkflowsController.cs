﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using PLM.Models;

namespace PLM.Controllers
{
    public class WorkflowsController : Controller
    {
        private readonly plmContext _context;
        private readonly IToastNotification _toastNotification;
        private readonly String _editMessage = "Workflow editat";
        private readonly String _addMessage = "Workflow adaugat";
        private readonly String _deleteMessage = "Workflow sters";
        private readonly InventoriesController _inventoriesController;
        private readonly ProductionhistoriesController _productionhistoriesController;
        public WorkflowsController(plmContext context, IToastNotification toastNotification, InventoriesController inventoriesController, ProductionhistoriesController productionhistoriesController)
        {
            _context = context;
            _toastNotification = toastNotification;
            _inventoriesController = inventoriesController;
            _productionhistoriesController = productionhistoriesController;
        }

        // GET: Workflows
        public async Task<IActionResult> Index()
        {
            var workflows = _context.Workflow.Include(w => w.Bom).Include(w => w.Equipment).Include(w => w.Staff).Include(w => w.Workflowstatus);
            
            foreach (var workf in workflows)
            {

                if (workf.WorkflowstatusId == 7)
                {
                    Inventory inv = new Inventory();
                
                    inv.Expectedqty = 1000;
                    inv.Availableqty = workf.Bom.Quantity;
                    inv.WorkflowId = workf.Id;
                    inv.WorkflowBomId = workf.BomId;
                    inv.WorkflowBomEquipmentId = workf.BomEquipmentId;
                    inv.WorkflowBomProductId = workf.BomProductId;
                    inv.WorkflowStaffId = workf.StaffId;
                    _context.Add(inv);
                    Productionhistory ph = new Productionhistory();                  
                    ph.WorkflowId = workf.Id;
                    ph.WorkflowBomId = workf.BomId;
                    ph.WorkflowBomEquipmentId = workf.BomEquipmentId;
                    ph.WorkflowBomProductId = workf.BomProductId;
                    ph.WorkflowStaffId = workf.StaffId;
                    ph.Name = workf.Details;
                    ph.Quantiy = workf.Bom.Quantity;
                    ph.Description = "Generated from workflow " + workf.Id;
                    _context.Add(ph);
                    workf.WorkflowstatusId = 9;
                }
            }
            _context.SaveChanges();
            //var plmContext = _context.Workflow.Include(w => w.Bom).Include(w => w.Equipment).Include(w => w.Staff).Include(w => w.Workflowstatus);
           
            return View( workflows.ToList());
        }

        // GET: Workflows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflow = await _context.Workflow
                .Include(w => w.Bom)
                .Include(w => w.Equipment)
                .Include(w => w.Staff)
                .Include(w => w.Workflowstatus)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflow == null)
            {
                return NotFound();
            }

            return View(workflow);
        }

        // GET: Workflows/Create
        public IActionResult Create()
        {
            ViewData["BomId"] = new SelectList(_context.Bom, "Id", "Name");
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name");
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            ViewData["WorkflowstatusId"] = new SelectList(_context.Workflowstatus, "Id", "Name");

            return View();
        }

        // POST: Workflows/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,StaffId,BomId,BomProductId,BomEquipmentId,Details,From,To,CreateTime,UpdateTime,WorkflowstatusId,EquipmentId")] Workflow workflow)
        {
            if (ModelState.IsValid)
            {
                var bom = _context.Bom.Where(b => b.Id == workflow.BomId).FirstOrDefault();
                workflow.BomEquipmentId = bom.EquipmentId;
                workflow.BomProductId = bom.ProductId;
                workflow.EquipmentId = bom.EquipmentId;
                _context.Add(workflow);
                await _context.SaveChangesAsync();
                _toastNotification.AddSuccessToastMessage(_addMessage);
                return RedirectToAction(nameof(Index));
            }
            ViewData["BomId"] = new SelectList(_context.Bom, "Id", "Id", workflow.BomId);
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", workflow.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", workflow.StaffId);
            ViewData["WorkflowstatusId"] = new SelectList(_context.Workflowstatus, "Id", "Name", workflow.WorkflowstatusId);
            return View(workflow);
        }

        // GET: Workflows/Edit/5
        public async Task<IActionResult> Edit(int? id, int? sid, int? bid, int? bpid, int? beid)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflow = await _context.Workflow.FindAsync(id, sid, bid, bpid, beid);
            if (workflow == null)
            {
                return NotFound();
            }
            ViewData["BomId"] = new SelectList(_context.Bom, "Id", "Id", workflow.BomId);
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", workflow.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Password", workflow.StaffId);
            ViewData["WorkflowstatusId"] = new SelectList(_context.Workflowstatus, "Id", "Name", workflow.WorkflowstatusId);
            return View(workflow);
        }

        // POST: Workflows/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,StaffId,BomId,BomProductId,BomEquipmentId,Details,From,To,CreateTime,UpdateTime,WorkflowstatusId,EquipmentId")] Workflow workflow)
        {
            if (id != workflow.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workflow);
                    _toastNotification.AddInfoToastMessage(_editMessage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkflowExists(workflow.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BomId"] = new SelectList(_context.Bom, "Id", "Id", workflow.BomId);
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", workflow.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", workflow.StaffId);
            ViewData["WorkflowstatusId"] = new SelectList(_context.Workflowstatus, "Id", "Name", workflow.WorkflowstatusId);
            return View(workflow);
        }

        // GET: Workflows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflow = await _context.Workflow
                .Include(w => w.Bom)
                .Include(w => w.Equipment)
                .Include(w => w.Staff)
                .Include(w => w.Workflowstatus)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflow == null)
            {
                return NotFound();
            }

            return View(workflow);
        }

        // POST: Workflows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int StaffId, int BomId, int BomProductId, int BomEquipmentId)
        {
            var workflow = await _context.Workflow.FindAsync(id, StaffId, BomId, BomProductId, BomEquipmentId);
            _context.Workflow.Remove(workflow);
            await _context.SaveChangesAsync();
            _toastNotification.AddWarningToastMessage(_deleteMessage);
            return RedirectToAction(nameof(Index));
        }

        private bool WorkflowExists(int id)
        {
            return _context.Workflow.Any(e => e.Id == id);
        }
        private bool InventoryWithWorkflowExists(int workflowId)
        {
            return _context.Inventory.Any(a => a.WorkflowId == workflowId);
        }
    }
}
