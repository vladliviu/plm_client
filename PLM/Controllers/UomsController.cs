﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class UomsController : Controller
    {
        private readonly plmContext _context;

        public UomsController(plmContext context)
        {
            _context = context;
        }

        // GET: Uoms
        public async Task<IActionResult> Index()
        {
            return View(await _context.Uom.ToListAsync());
        }

        // GET: Uoms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uom = await _context.Uom
                .FirstOrDefaultAsync(m => m.Id == id);
            if (uom == null)
            {
                return NotFound();
            }

            return View(uom);
        }

        // GET: Uoms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Uoms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Units,Type,CreateTime,UpdateTime")] Uom uom)
        {
            if (ModelState.IsValid)
            {
                _context.Add(uom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(uom);
        }

        // GET: Uoms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uom = await _context.Uom.FindAsync(id);
            if (uom == null)
            {
                return NotFound();
            }
            return View(uom);
        }

        // POST: Uoms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Units,Type,CreateTime,UpdateTime")] Uom uom)
        {
            if (id != uom.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UomExists(uom.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(uom);
        }

        // GET: Uoms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uom = await _context.Uom
                .FirstOrDefaultAsync(m => m.Id == id);
            if (uom == null)
            {
                return NotFound();
            }

            return View(uom);
        }

        // POST: Uoms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uom = await _context.Uom.FindAsync(id);
            _context.Uom.Remove(uom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UomExists(int id)
        {
            return _context.Uom.Any(e => e.Id == id);
        }
    }
}
