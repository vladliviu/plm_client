﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class StaffoccupationsController : Controller
    {
        private readonly plmContext _context;

        public StaffoccupationsController(plmContext context)
        {
            _context = context;
        }

        // GET: Staffoccupations
        public async Task<IActionResult> Index()
        {
            return View(await _context.Staffoccupation.ToListAsync());
        }

        // GET: Staffoccupations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staffoccupation = await _context.Staffoccupation
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staffoccupation == null)
            {
                return NotFound();
            }

            return View(staffoccupation);
        }

        // GET: Staffoccupations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Staffoccupations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Salary,ShiftDuration,ShiftFrom,ShiftTo,CreateTime,UpdateTime")] Staffoccupation staffoccupation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(staffoccupation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(staffoccupation);
        }

        // GET: Staffoccupations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staffoccupation = await _context.Staffoccupation.FindAsync(id);
            if (staffoccupation == null)
            {
                return NotFound();
            }
            return View(staffoccupation);
        }

        // POST: Staffoccupations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Salary,ShiftDuration,ShiftFrom,ShiftTo,CreateTime,UpdateTime")] Staffoccupation staffoccupation)
        {
            if (id != staffoccupation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(staffoccupation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffoccupationExists(staffoccupation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(staffoccupation);
        }

        // GET: Staffoccupations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staffoccupation = await _context.Staffoccupation
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staffoccupation == null)
            {
                return NotFound();
            }

            return View(staffoccupation);
        }

        // POST: Staffoccupations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staffoccupation = await _context.Staffoccupation.FindAsync(id);
            _context.Staffoccupation.Remove(staffoccupation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StaffoccupationExists(int id)
        {
            return _context.Staffoccupation.Any(e => e.Id == id);
        }
    }
}
