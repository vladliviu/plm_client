﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class WastesController : Controller
    {
        private readonly plmContext _context;
        private readonly InventoriesController _inventoriesController;
        private readonly ProductsController _productsController;

        public WastesController(plmContext context, InventoriesController inventoriesController, ProductsController productsController)
        {
            _context = context;
            _inventoriesController = inventoriesController;
            _productsController = productsController;
        }

        // GET: Wastes
        public async Task<IActionResult> Index()
        {
            var plmContext = _context.Waste.Include(w => w.Inventory);
            return View(await plmContext.ToListAsync());
        }

        // GET: Wastes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var waste = await _context.Waste
                .Include(w => w.Inventory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (waste == null)
            {
                return NotFound();
            }

            return View(waste);
        }

        // GET: Wastes/Create
        public IActionResult Create()
        {
            ViewData["InventoryId"] = new SelectList(_context.Inventory, "Id", "Id");
            return View();
        }

        // POST: Wastes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Details,Quantity,Value,CreateTime,UpdateTime,InventoryId")] Waste waste)
        {
            var inv = _context.Inventory.Where(i => i.Id == waste.InventoryId).FirstOrDefault();
            var prod = _context.Product.Where(p => p.Id == inv.WorkflowBomProductId).FirstOrDefault();


            if (ModelState.IsValid && waste.Quantity <= inv.Availableqty)
            {
                prod.Quantity = prod.Quantity + waste.Quantity * 0.75;
                inv.Availableqty -= waste.Quantity;
                //_context.Inventory.Remove(inv);
                waste.Value = waste.Quantity * 0.25;
                _context.Add(waste);
                _context.Inventory.Update(inv);
                _context.Product.Update(prod);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["InventoryId"] = new SelectList(_context.Inventory, "Id", "Id", waste.InventoryId);
            return View(waste);
        }

        // GET: Wastes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var waste = await _context.Waste.FindAsync(id);
            if (waste == null)
            {
                return NotFound();
            }
            ViewData["InventoryId"] = new SelectList(_context.Inventory, "Id", "Id", waste.InventoryId);
            return View(waste);
        }

        // POST: Wastes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Details,Quantity,Value,CreateTime,UpdateTime,InventoryId")] Waste waste)
        {
            if (id != waste.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(waste);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WasteExists(waste.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["InventoryId"] = new SelectList(_context.Inventory, "Id", "Id", waste.InventoryId);
            return View(waste);
        }

        // GET: Wastes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var waste = await _context.Waste
                .Include(w => w.Inventory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (waste == null)
            {
                return NotFound();
            }

            return View(waste);
        }

        // POST: Wastes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var waste = await _context.Waste.FindAsync(id);
            _context.Waste.Remove(waste);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WasteExists(int id)
        {
            return _context.Waste.Any(e => e.Id == id);
        }
    }
}
