﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class WorkflowstatusController : Controller
    {
        private readonly plmContext _context;

        public WorkflowstatusController(plmContext context)
        {
            _context = context;
        }

        // GET: Workflowstatus
        public async Task<IActionResult> Index()
        {
            return View(await _context.Workflowstatus.ToListAsync());
        }

        // GET: Workflowstatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowstatus = await _context.Workflowstatus
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflowstatus == null)
            {
                return NotFound();
            }

            return View(workflowstatus);
        }

        // GET: Workflowstatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Workflowstatus/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,CreateTime,UpdateTime")] Workflowstatus workflowstatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(workflowstatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(workflowstatus);
        }

        // GET: Workflowstatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowstatus = await _context.Workflowstatus.FindAsync(id);
            if (workflowstatus == null)
            {
                return NotFound();
            }
            return View(workflowstatus);
        }

        // POST: Workflowstatus/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,CreateTime,UpdateTime")] Workflowstatus workflowstatus)
        {
            if (id != workflowstatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(workflowstatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkflowstatusExists(workflowstatus.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(workflowstatus);
        }

        // GET: Workflowstatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workflowstatus = await _context.Workflowstatus
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workflowstatus == null)
            {
                return NotFound();
            }

            return View(workflowstatus);
        }

        // POST: Workflowstatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workflowstatus = await _context.Workflowstatus.FindAsync(id);
            _context.Workflowstatus.Remove(workflowstatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkflowstatusExists(int id)
        {
            return _context.Workflowstatus.Any(e => e.Id == id);
        }
    }
}
