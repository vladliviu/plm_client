﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using PLM.Models;

namespace PLM.Controllers
{
    public class StaffsController : Controller
    {
        private readonly plmContext _context;
        private readonly IToastNotification _toastNotification;

        public StaffsController(plmContext context, IToastNotification toastNotification)
        {
            _context = context;
            _toastNotification = toastNotification;
        }

        public IActionResult Login()
        {
            //Testing Default Methods

            //Success
         //   _toastNotification.AddSuccessToastMessage();
            //Info
          //  _toastNotification.AddInfoToastMessage();
            //Warning
          //  _toastNotification.AddWarningToastMessage();
            //Error
          //  _toastNotification.AddErrorToastMessage();

            TempData["username"] = "";
            TempData["userid"] = 0;
            TempData["occupationid"] = 0;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(String Username, String Password)
        {
            var staffLoginInfo = _context.Staff.Where(s => s.Username == Username && s.Password == Password).FirstOrDefault();
           // var shiftLoginInfo = _context.Staffoccupation.Where(s => s.Id == staffLoginInfo.StaffOccupationId).FirstOrDefault();

            if (staffLoginInfo == null)
            {
                _toastNotification.AddErrorToastMessage("Utilizatorul sau parola este gresita!");
                return (RedirectToAction(nameof(Login)));
            } 
         //   else if (shiftLoginInfo.ShiftFrom.Value.Hour > DateTime.Now.Hour || shiftLoginInfo.ShiftTo.Value.Hour < DateTime.Now.Hour)
          //  {
          //      _toastNotification.AddWarningToastMessage("Utilizatorul nu poate intra in shift!");
          //      return (RedirectToAction(nameof(Login)));
           // }
            else
            {
                //_toastNotification.AddSuccessToastMessage("User-ul " + staffLoginInfo.Username + " s-a logat cu success!");
                _toastNotification.AddSuccessToastMessage("User-ul s-a logat cu success!");
                TempData["username"] = staffLoginInfo.Username;
                TempData["userid"] = staffLoginInfo.Id;
                TempData["occupationid"] = staffLoginInfo.StaffOccupationId;
                TempData["firstname"] = staffLoginInfo.Firstname;
                TempData["lastname"] = staffLoginInfo.Lastname;
                return RedirectToAction("Details", "Staffs", new { id = staffLoginInfo.Id });
            }
        }

        public IActionResult Logout()
        {
            _toastNotification.AddInfoToastMessage("Ati fost delogat");
            TempData["username"] = "";
            TempData["userid"] = 0;
            TempData["occupationid"] = 0;
            return RedirectToAction(nameof(Login));
        }

        // GET: Staffs
        public async Task<IActionResult> Index()
        {
            var plmContext = _context.Staff.Include(s => s.StaffOccupation);
            return View(await plmContext.ToListAsync());
        }

        // GET: Staffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            TempData["notCount"] = _context.Notification.Where(d=> d.Status == "Active").Count();
            TempData["downCount"] = _context.Downtime.Where(d => d.Status == "Active").Count();

            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .Include(s => s.StaffOccupation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // GET: Staffs/Create
        public IActionResult Create()
        {
            ViewData["StaffOccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Name");
            return View();
        }

        // POST: Staffs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Password,Firstname,Lastname,Email,Address,Phone,Avatar,CreateTime,UpdateTime,StaffOccupationId")] Staff staff)
        {
            if (ModelState.IsValid)
            {
                _context.Add(staff);
                await _context.SaveChangesAsync();
                _toastNotification.AddSuccessToastMessage("Angajatul " + staff.Firstname + ", " + staff.Lastname + " a fost adaugat cu success");
                return RedirectToAction(nameof(Index));
            }
            ViewData["StaffOccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Name", staff.StaffOccupationId);
            return View(staff);
        }

        // GET: Staffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff.FindAsync(id);
            if (staff == null)
            {
                return NotFound();
            }
            ViewData["StaffOccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Name", staff.StaffOccupationId);
            return View(staff);
        }

        // POST: Staffs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password,Firstname,Lastname,Email,Address,Phone,Avatar,CreateTime,UpdateTime,StaffOccupationId")] Staff staff)
        {
            if (id != staff.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(staff);
                    await _context.SaveChangesAsync();
                    _toastNotification.AddSuccessToastMessage("Angajatul a fost editat cu success");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffExists(staff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StaffOccupationId"] = new SelectList(_context.Staffoccupation, "Id", "Name", staff.StaffOccupationId);
            return View(staff);
        }

        // GET: Staffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .Include(s => s.StaffOccupation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // POST: Staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staff = await _context.Staff.FindAsync(id);
            _context.Staff.Remove(staff);
            await _context.SaveChangesAsync();
            _toastNotification.AddSuccessToastMessage("Angajatul a fost sters cu success");
            return RedirectToAction(nameof(Index));
        }

        private bool StaffExists(int id)
        {
            return _context.Staff.Any(e => e.Id == id);
        }
    }
}
