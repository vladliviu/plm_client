﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class BomsController : Controller
    {
        private readonly plmContext _context;

        public BomsController(plmContext context)
        {
            _context = context;
        }

        // GET: Boms
        public async Task<IActionResult> Index()
        {
            var plmContext = _context.Bom.Include(b => b.Equipment).Include(b => b.Product);
            return View(await plmContext.ToListAsync());
        }

        // GET: Boms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bom = await _context.Bom
                .Include(b => b.Equipment)
                .Include(b => b.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bom == null)
            {
                return NotFound();
            }

            return View(bom);
        }

        // GET: Boms/Create
        public IActionResult Create()
        {
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name");
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name");
            return View();
        }

        // POST: Boms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,EquipmentId,Name,Description,Quantity,CreateTime,UpdateTime")] Bom bom)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", bom.EquipmentId);
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", bom.ProductId);
            return View(bom);
        }

        // GET: Boms/Edit/5
        public async Task<IActionResult> Edit(int? id, int? eid, int? pid)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bom = await _context.Bom.FindAsync(id, pid, eid);
            if (bom == null)
            {
                return NotFound();
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", bom.EquipmentId);
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", bom.ProductId);
            return View(bom);
        }

        // POST: Boms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,EquipmentId,Name,Description,Quantity,CreateTime,UpdateTime")] Bom bom)
        {
            if (id != bom.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BomExists(bom.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", bom.EquipmentId);
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", bom.ProductId);
            return View(bom);
        }

        // GET: Boms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bom = await _context.Bom
                .Include(b => b.Equipment)
                .Include(b => b.Product)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bom == null)
            {
                return NotFound();
            }

            return View(bom);
        }

        // POST: Boms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int ProductId, int EquipmentId)
        {
            var bom = await _context.Bom.FindAsync(id, ProductId, EquipmentId);
            _context.Bom.Remove(bom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BomExists(int id)
        {
            return _context.Bom.Any(e => e.Id == id);
        }
    }
}
