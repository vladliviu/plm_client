﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PLM.Models;
using Syncfusion.EJ2.Charts;

namespace WorkflowManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly plmContext _context;

        public HomeController(ILogger<HomeController> logger, plmContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            TempData["username"] = "";
            TempData["userid"] = 0;
            TempData["occupationid"] = 0;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult WasteChart()
        {
            var dataWaste = _context.Waste.ToList();
            return Json(dataWaste);
        }
        public IActionResult Column()
        {
            List<ColumnChartData> chartData = new List<ColumnChartData>
            {
                new ColumnChartData { x= "USA", yValue= 46, yValue1=37, yValue2=38 },
                new ColumnChartData { x= "GBR", yValue= 27, yValue1=23, yValue2=17 },
                new ColumnChartData { x= "CHN", yValue= 26, yValue1=18, yValue2=26 }
            };
            ViewBag.dataSource = chartData;
            return View();
        }
        public class ColumnChartData
        {
            public string x;
            public double yValue;
            public double yValue1;
            public double yValue2;
        }
    }
}
}
