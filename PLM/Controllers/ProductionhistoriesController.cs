﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PLM.Models;

namespace PLM.Controllers
{
    public class ProductionhistoriesController : Controller
    {
        private readonly plmContext _context;

        public ProductionhistoriesController(plmContext context)
        {
            _context = context;
        }

        // GET: Productionhistories
        public async Task<IActionResult> Index()
        {
            var plmContext = _context.Productionhistory.Include(p => p.Workflow);
            return View(await plmContext.ToListAsync());
        }

        // GET: Productionhistories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productionhistory = await _context.Productionhistory
                .Include(p => p.Workflow)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productionhistory == null)
            {
                return NotFound();
            }

            return View(productionhistory);
        }

        // GET: Productionhistories/Create
        public IActionResult Create()
        {
            ViewData["WorkflowId"] = new SelectList(_context.Workflow, "Id", "Id");
            return View();
        }

        // POST: Productionhistories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Quantiy,Price,CreateTime,UpdateTime,WorkflowId,WorkflowStaffId,WorkflowBomId,WorkflowBomProductId,WorkflowBomEquipmentId")] Productionhistory productionhistory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productionhistory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["WorkflowId"] = new SelectList(_context.Workflow, "Id", "Id", productionhistory.WorkflowId);
            return View(productionhistory);
        }

        // GET: Productionhistories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productionhistory = await _context.Productionhistory.FindAsync(id);
            if (productionhistory == null)
            {
                return NotFound();
            }
            ViewData["WorkflowId"] = new SelectList(_context.Workflow, "Id", "Id", productionhistory.WorkflowId);
            return View(productionhistory);
        }

        // POST: Productionhistories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Quantiy,Price,CreateTime,UpdateTime,WorkflowId,WorkflowStaffId,WorkflowBomId,WorkflowBomProductId,WorkflowBomEquipmentId")] Productionhistory productionhistory)
        {
            if (id != productionhistory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productionhistory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductionhistoryExists(productionhistory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["WorkflowId"] = new SelectList(_context.Workflow, "Id", "Id", productionhistory.WorkflowId);
            return View(productionhistory);
        }

        // GET: Productionhistories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productionhistory = await _context.Productionhistory
                .Include(p => p.Workflow)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productionhistory == null)
            {
                return NotFound();
            }

            return View(productionhistory);
        }

        // POST: Productionhistories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productionhistory = await _context.Productionhistory.FindAsync(id);
            _context.Productionhistory.Remove(productionhistory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductionhistoryExists(int id)
        {
            return _context.Productionhistory.Any(e => e.Id == id);
        }
    }
}
