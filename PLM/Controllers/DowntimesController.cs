﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using PLM.Models;

namespace PLM.Controllers
{
    public class DowntimesController : Controller
    {
        private readonly plmContext _context;
        private readonly IToastNotification _toastNotification;
        private readonly WorkflowsController _workflowsController;

        public DowntimesController(plmContext context, IToastNotification toastNotification, WorkflowsController workflowsController)
        {
            _context = context;
            _toastNotification = toastNotification;
            _workflowsController = workflowsController;
        }

        // GET: Downtimes
        public async Task<IActionResult> Index()
        {
            TempData["notCount"] = _context.Notification.Where(d => d.Status == "Active").Count();
            TempData["downCount"] = _context.Downtime.Where(d => d.Status == "Active").Count();
            var plmContext = _context.Downtime.Include(d => d.Equipment).Include(d => d.Staff);
            return View(await plmContext.ToListAsync());
        }

        // GET: Downtimes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtime = await _context.Downtime
                .Include(d => d.Equipment)
                .Include(d => d.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (downtime == null)
            {
                return NotFound();
            }

            return View(downtime);
        }

        // GET: Downtimes/Create
        public IActionResult Create()
        {
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name");
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            return View();
        }

        // POST: Downtimes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Type,Description,Status,CreatedBy,CreateTime,UpdateTime,StaffId,EquipmentId")] Downtime downtime)
        {
            if (ModelState.IsValid)
            {
                downtime.Createdby = (string)TempData["username"];
                downtime.Status = "Active";

                _context.Add(downtime);
                _toastNotification.AddSuccessToastMessage("Downtime adaugat cu succes");
                await _context.SaveChangesAsync();
                
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtime.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtime.StaffId);
            return View(downtime);
        }

        // GET: Downtimes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtime = await _context.Downtime.FindAsync(id);
            if (downtime == null)
            {
                return NotFound();
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtime.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtime.StaffId);
            return View(downtime);
        }

        // POST: Downtimes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Type,Description,CreateTime,UpdateTime,StaffId,EquipmentId")] Downtime downtime)
        {
            if (id != downtime.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(downtime);
                    _toastNotification.AddSuccessToastMessage("Downtime editat cu succes");
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DowntimeExists(downtime.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtime.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtime.StaffId);
            return View(downtime);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResolveDowntime(int did)
        {
            _toastNotification.AddSuccessToastMessage("Downtime rezolvat");
            var downtime = _context.Downtime.Where(d => d.Id == did).FirstOrDefault();
          
            if (ModelState.IsValid)
            {
                try
                {
                    downtime.Status = "Resolved";
                    _context.Update(downtime);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DowntimeExists(downtime.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EquipmentId"] = new SelectList(_context.Equipment, "Id", "Name", downtime.EquipmentId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", downtime.StaffId);
            return View(downtime);
        }
        // GET: Downtimes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var downtime = await _context.Downtime
                .Include(d => d.Equipment)
                .Include(d => d.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (downtime == null)
            {
                return NotFound();
            }

            return View(downtime);
        }

        // POST: Downtimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var downtime = await _context.Downtime.FindAsync(id);
            _context.Downtime.Remove(downtime);
            _toastNotification.AddSuccessToastMessage("Downtime sters cu succes");
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DowntimeExists(int id)
        {
            return _context.Downtime.Any(e => e.Id == id);
        }

        private void CheckEquipment(Downtime downtime)
        {
            var changeWorkflowStatus = _context.Workflow.Where(w => w.EquipmentId == downtime.EquipmentId).ToList();
            foreach(var cws in changeWorkflowStatus)
            {
                cws.WorkflowstatusId = 8;
            }
        }
    }
}
