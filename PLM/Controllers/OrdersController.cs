﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using PLM.Models;

namespace PLM.Controllers
{
    public class OrdersController : Controller
    {
        private readonly plmContext _context;
        private readonly IToastNotification _toastNotification;
        private readonly ProductsController _productsController;

        public OrdersController(plmContext context, IToastNotification toastNotification, ProductsController productsController)
        {
            _context = context;
            _toastNotification = toastNotification;
            _productsController = productsController;
        }

        // GET: Orders
        public async Task<IActionResult> Index()
        {
            var plmContext = _context.Order.Include(o => o.Product).Include(o => o.Supplier);
            return View(await plmContext.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .Include(o => o.Product)
                .Include(o => o.Supplier)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        public IActionResult Create()
        {
 
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name");
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name");
           
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,SupplierId,Quantity,Details,CreateTime,UpdateTime")] Order order)
        {
            Product prod = new Product();
            var supplier = _context.Supplier.Where(s => s.Id == order.SupplierId).FirstOrDefault();
            if(order.ProductId.ToString() != supplier.Productcode.ToString())
            {
                _toastNotification.AddWarningToastMessage("Furnizorul nu dispune de acest produs");
            }
            else if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                prod = _context.Product.Where(p => p.Id == order.ProductId).FirstOrDefault();
                prod.Quantity += order.Quantity;
                await _productsController.Edit(order.ProductId, prod);
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", order.ProductId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Name", order.SupplierId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", order.ProductId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Id", order.SupplierId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,SupplierId,Quantity,Details,CreateTime,UpdateTime")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Product, "Id", "Name", order.ProductId);
            ViewData["SupplierId"] = new SelectList(_context.Supplier, "Id", "Id", order.SupplierId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .Include(o => o.Product)
                .Include(o => o.Supplier)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.Id == id);
        }
    }
}
